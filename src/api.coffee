module.exports = ( options ) ->

  @add 'role:api,path:update', ( msg, respond ) ->
    console.log msg.args
    respond null

  @add 'init:api', ( msg, respond ) ->
    @act 'role:web',
      routes:
        prefix: '/update'
        pin: 'role:api,path:*'
        map:
          update:
            POST: true
            name: ''
    , respond
