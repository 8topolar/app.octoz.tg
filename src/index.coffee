SenecaWeb = require 'seneca-web'
Express = require 'express'
Router = Express.Router
context = new Router()

senecaWebConfig =
  context: context
  adapter: require 'seneca-web-adapter-express'
  options:
    parseBody: false

app = Express()
  .use Express.json()
  .use context
  .listen 9000

seneca = require( 'seneca' )()
  .use SenecaWeb, senecaWebConfig
  .use 'api'
  .client type: 'tcp', pin: 'role:api'

# seneca = require( 'seneca' )()
#   .use 'color'
#   .ready () ->
#     console.log('READY')
# 
# # app = express()
# #   .use express.json()
# #   .use seneca.export 'web'
# #   .listen 9000
